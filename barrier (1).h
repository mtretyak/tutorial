#include <iostream>
#include <mutex>
#include <condition_variable>

using namespace std;

class barrier {
public:
	explicit barrier(size_t num_threads): capacity(num_threads), size(0), is_blocked(true) {}
	void enter();

private:
	size_t capacity;
	size_t size;
	condition_variable barrier1, barrier2;
	mutex barrier_mutex;
	bool is_blocked;

};

void barrier::enter() {
	unique_lock<mutex> lock(barrier_mutex);
	barrier1.wait(lock, [this] (){return is_blocked;});
	size++;
	barrier2.wait(lock, [this] () {return capacity == size || !is_blocked; });
	is_blocked = false;
	--size;
	barrier2.notify_all();
	if (!size) {
        is_blocked = true;
		barrier1.notify_all();
	}
}

