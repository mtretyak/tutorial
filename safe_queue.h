#include<iostream>
#include<queue>
#include<vector>
#include<thread>
#include <condition_variable>
#include <mutex>

using namespace std;

template<class T>
class thread_safe_queue{
public:
    thread_safe_queue(size_t _capacity, T _poison_item):capacity(_capacity),flag(false), is_poisoned(false), poison_item(_poison_item){};//������ ������, ����� ���� poison, ���� shutdown(� ������� �������)
    bool enqueue(T item);
    bool pop(T& item);
    void shutdown();
    void poison();
private:
    bool flag;//shutdown
    bool is_poisoned;
    T poison_item;
    queue<T> safe_queue;
    size_t capacity;
    condition_variable is_full;
    condition_variable is_empty;
    mutex queue_mutex;
};

template<class T>
bool thread_safe_queue<T>::enqueue(T item){
    if (flag) return flag;
    if (is_poisoned) item = poison_item;
    unique_lock<mutex> lock(queue_mutex);
    is_full.wait(lock, [this] () {return safe_queue.size() != capacity;});
    safe_queue.push(item);
    is_empty.notify_one();
    return false;
}

template<class T> //true - ������, ��� ���� shutdowm, ���� poison
bool thread_safe_queue<T>::pop(T& item){
    if (flag) return flag;
    if (item == poison_item) return true;
    unique_lock<mutex> lock(queue_mutex);
    is_empty.wait(lock, [this] () {return !safe_queue.empty(); });
    cout << safe_queue.front() << endl;
    item = safe_queue.front();
    safe_queue.pop();
    is_full.notify_one();
    return false;
}

template<class T>
void thread_safe_queue<T>::shutdown(){
    flag = true;
}

template<class T>
void thread_safe_queue<T>::poison(){
    is_poisoned = true;
}
