#include<iostream>
#include<queue>
#include<vector>
#include<thread>
#include <condition_variable>
#include <mutex>

using namespace std;

template<class T>
class thread_safe_queue{
public:
    thread_safe_queue():flag(false), is_enable(true){};//������ ������, ����� ���� poison, ���� shutdown(� ������� �������)
    bool enqueue(T item);
    bool pop(T& item);
    void shutdown();
    bool get_is_enable(){ return is_enable; }
    void set_is_enable(bool a){ is_enable = a; }
    void get_all(){ is_empty.notify_all(); }
    bool isn_empty(){return safe_queue.empty();}
private:
    bool flag;//shutdown
    bool is_enable;
    queue<T> safe_queue;
    condition_variable is_empty;
    mutex queue_mutex;
};

template<class T>
bool thread_safe_queue<T>::enqueue(T item){
    if (flag) return flag;
    unique_lock<mutex> lock(queue_mutex);
    safe_queue.push(move(item));
    is_empty.notify_one();
    return false;
}

template<class T> //true - ������, ��� ���� shutdowm, ���� poison
bool thread_safe_queue<T>::pop(T& item){
    if (flag) return flag;
    unique_lock<mutex> lock(queue_mutex);
    is_empty.wait(lock, [this] () {return !safe_queue.empty() || !is_enable; });
//    cout << safe_queue.front() << endl;
    item = move(safe_queue.front());
    safe_queue.pop();
    return false;
}

template<class T>
void thread_safe_queue<T>::shutdown(){
    flag = true;
}

