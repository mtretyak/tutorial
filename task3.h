#include<thread>
#include<iostream>
#include<vector>
#include<atomic>
#include<array>

using namespace std;

class peterson_mutex{
public:
    peterson_mutex(){
        want[0].store(false);
        want[1].store(false);
        victim.store(0);
    }
    void lock(int t){
        want[t].store(true);
        victim.store(t);
        while (want[1 - t].load() && victim.load() == t){
          std::this_thread::yield();
        }
    }
    void unlock(int t){
        want[t].store(false);
    }
private:
    array<atomic<bool>, 2> want;
    atomic<int> victim;
};

class tree_mutex{
public:
    tree_mutex(size_t num_threads):tree(2* pow(num_threads) - 1){
        _num_threads = num_threads;
    }
    void lock(size_t thread_index);
    void unlock(size_t thread_index);
private:
    vector<peterson_mutex> tree;
    size_t k, _num_threads;;
    size_t pow(size_t num_threads){
        k = 1;
        while(k < num_threads){
            k *= 2;
        }
        if (k != 1)
            k = k >> 1;
        return k;
    }
};

void tree_mutex::lock(size_t thread_index){
    size_t id = 2*k - 1 + thread_index;
    while(id != 0){
        tree[(id - 1) >> 1].lock((id + 1) & 1);
        id = (id - 1) >> 1;

    }
}

void tree_mutex::unlock(size_t thread_index) {
    size_t depth = 0;
    while ((1 << depth) < _num_threads) depth++; //������� ������
    size_t id = 0;
    for (int i = depth - 1; i >= 0; i--) {
        tree[id].unlock((thread_index >> i) & 1);// ��� �� ���� � ����� ����� � �� ������ - ������ ������
        id = id * 2 + 1 + ((thread_index >> i) & 1);
    }
}
