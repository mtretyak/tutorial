#include<iostream>
#include<queue>
#include<vector>
#include<thread>
#include <condition_variable>
#include <mutex>
#include <future>
#include <functional>
#include "safe_queue_indef.h"
#include "thread_pool.h"


int main(){
    const int n = 5;
    thread_pool<int> pool(10);
    vector< future<int> > async_result(n);
    for(int i = 0; i < n; ++i){
    function<int()> f = bind(print,i);
        async_result[i] = pool.submit(move(f));
    }
    for(int i = 0; i < n; ++i){
        int a = async_result[i].get();
    }
    pool.shutdown();
    return 0;
}

//g++ thread_pool.cpp -o main.out -pthread -std=c++11
