#include<iostream>
#include<queue>
#include<vector>
#include<thread>
#include <condition_variable>
#include <mutex>
#include <exception>
#include <future>
#include <limits>

using namespace std;

const size_t cap = numeric_limits<size_t>::max();

template <class Value, class Container = std::deque<Value>>
class thread_safe_queue{
public:
    thread_safe_queue(size_t _capacity = cap){ capacity = _capacity; flag = false;}
    void enqueue(Value item);
    bool pop(Value& item);
    void shutdown();
    bool empty(){return safe_queue.empty();}
    size_t size(){return safe_queue.size();}
private:
    bool flag;//shutdown
    Container safe_queue;
    size_t capacity;
    condition_variable is_full;
    condition_variable is_empty;
    mutex queue_mutex;
};

class MyException: public exception{
    virtual const char* what() const throw()
    {
        return "ololo";
    }
};

template <class Value, class Container>
void thread_safe_queue<Value, Container>::enqueue(Value item){
    unique_lock<mutex> lock(queue_mutex);
    is_full.wait(lock, [this] () {return safe_queue.size() != capacity || flag;});
    if (flag) throw MyException();
    safe_queue.push_back(move(item));
    is_empty.notify_one();
}

template <class Value, class Container>
bool thread_safe_queue<Value, Container>::pop(Value& item){
    unique_lock<mutex> lock(queue_mutex);
    is_empty.wait(lock, [this] () {return !safe_queue.empty() || flag; });
    if (flag && safe_queue.empty()) return false;
    item = move(safe_queue.front());
    safe_queue.pop_front();
    is_full.notify_one();
    return true;
}

template <class Value, class Container>
void thread_safe_queue<Value, Container>::shutdown(){
    flag = true;
    is_empty.notify_all();
    is_full.notify_all();
}

template<class T>
class thread_pool{
public:
    thread_pool(){
        size_t count = thread::hardware_concurrency();
        if (count == 0)
            count = 10;
        for(size_t i = 0; i < count; ++i)
            threads.emplace_back([this] () { start_thread(); });

    }
    explicit thread_pool(size_t _num_threads){
        flag = true;
        for(size_t i = 0; i < _num_threads; ++i){
            threads.emplace_back([this] () { start_thread(); });
        }
    }
    future<T> submit(function<T()> func);
    void shutdown();
private:
    bool flag;
    mutex pool_mutex;
    condition_variable is_shutdown;
    thread_safe_queue< packaged_task<T()> > queue;
    vector<thread> threads;
    void join_threads();
    void start_thread(){
        packaged_task<T()> func;
        while(queue.pop(func))
            func();
    }
};

template<typename T>
void thread_pool<T>::join_threads(){
    for (size_t i = 0; i < threads.size(); ++i){
		if (threads[i].joinable())
			threads[i].join();
    }
}


template<class T>
future<T> thread_pool<T>::submit(function<T()> func){
    if (!flag) throw MyException();
    packaged_task<T()> task(func);
    future<T> result = task.get_future();
    queue.enqueue(move(task));
    return result;
}

template<class T>
void thread_pool<T>::shutdown(){
    queue.shutdown();
    flag = false;
    join_threads();
}
