using namespace std;


template<class T>
class thread_pool{
public:
    thread_pool(){
        int count = thread::hardware_concurrency();
        if (count == 0)
            count = 10;
        for(int i = 0; i < count; ++i)
            threads.emplace_back(start_threads);
        for(int i = 0; i < threads.size(); ++i)
            threads[i].join();

    }
    thread_pool(int _num_threads){
        for(int i = 0; i < _num_threads; ++i){
            threads.emplace_back([this] () { start_threads(); });

        }
    }
    future<T> submit(function<T()> func);
    void shutdown();
    ~thread_pool();
private:
    thread_safe_queue< packaged_task<T()> > queue;
    vector<thread> threads;
    mutex thread_mutex;
    void join_threads();
    void start_threads(){
        while(queue.get_is_enable()){
            packaged_task<T()> f;
            queue.pop(f);
            if (!queue.get_is_enable())
                return;
            f();
        }
    }
};

template<typename T>
void thread_pool<T>::join_threads(){
    for (int i = 0; i < threads.size(); ++i){
		if (threads[i].joinable())
			threads[i].join();
    }
}

template<typename T>
thread_pool<T>::~thread_pool(){
    join_threads();
    packaged_task<T()> f;
}

template<class T>
future<T> thread_pool<T>::submit(function<T()> func){
    packaged_task<T()> task(func);
    future<T> result = task.get_future();
    queue.enqueue(move(task));
    return result;
}

template<class T>
void thread_pool<T>::shutdown(){
    queue.set_is_enable(false);
    queue.get_all();
}

int print(int a){
    cout << "a" << endl;
    return a * a;
}
