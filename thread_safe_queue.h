#include<iostream>
#include<queue>
#include<vector>
#include<thread>
#include <condition_variable>
#include <mutex>
#include <exception>


using namespace std;

template <class Value, class Container = std::deque<Value>>
class thread_safe_queue{
public:
    thread_safe_queue(size_t _capacity){ capacity = _capacity; flag = false;}
    void enqueue(Value item);
    void pop(Value& item);
    void shutdown();
private:
    bool flag;//shutdown
    Container safe_queue;
    size_t capacity;
    condition_variable is_full;
    condition_variable is_empty;
    mutex queue_mutex;
};

class MyException: public exception{
    virtual const char* what() const throw()
    {
        return "ololo";
    }
};

template <class Value, class Container>
void thread_safe_queue<Value, Container>::enqueue(Value item){
    unique_lock<mutex> lock(queue_mutex);
    is_full.wait(lock, [this] () {return safe_queue.size() != capacity || flag;});
    if (flag) throw MyException();
    safe_queue.push_back(move(item));
    is_empty.notify_one();
}

template <class Value, class Container>
void thread_safe_queue<Value, Container>::pop(Value& item){
    unique_lock<mutex> lock(queue_mutex);
    is_empty.wait(lock, [this] () {return !safe_queue.empty() || flag; });
    if (flag && safe_queue.empty()) throw MyException();
    item = move(safe_queue.front());
    safe_queue.pop_front();
    is_full.notify_one();
}

template <class Value, class Container>
void thread_safe_queue<Value, Container>::shutdown(){
    flag = true;
    is_empty.notify_all();
    is_full.notify_all();
}
